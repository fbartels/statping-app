FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

EXPOSE 8080

RUN apt-get update && \
	apt-get install -y sass && \
	rm -rf /var/cache/apt /var/lib/apt/lists

WORKDIR /app/pkg
ARG STATPING_VERSION=0.90.60
RUN curl -L https://github.com/statping/statping/releases/download/v$STATPING_VERSION/statping-linux-amd64.tar.gz | tar -xz -f - && \
	echo "Statping is verion $(/app/pkg/statping version)"

#RUN mkdir -p /app/pkg/ /app/data/ && ln -sfn /app/data/assets /app/pkg/assets

COPY start.sh /app/pkg/

WORKDIR /app/data

CMD [ "/app/pkg/start.sh" ]
