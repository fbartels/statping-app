#!/bin/bash

set -eux

export DB_CONN=postgres
export DB_HOST=$CLOUDRON_POSTGRESQL_HOST
export DB_PORT=$CLOUDRON_POSTGRESQL_PORT
export DB_USER=$CLOUDRON_POSTGRESQL_USERNAME
export DB_PASS=$CLOUDRON_POSTGRESQL_PASSWORD
export DB_DATABASE=$CLOUDRON_POSTGRESQL_DATABASE
export SAMPLE_DATA=true # do not install statping demo data
export IS_DOCKER=true
export STATPING_DIR=/app/data

random_string() {
        LC_CTYPE=C tr -dc 'a-zA-Z0-9' < /dev/urandom | head -c32
}

if [ ! -e /app/data/.env ]; then
        cat <<-EOF > "/app/data/.env"
# Config file to load
NAME="Statping"
DESCRIPTION="Status overview"
#USE_CDN=false
DISABLE_LOGS=false
ALLOW_REPORTS=false
API_SECRET=$(random_string)
EOF
fi

# export all values from ".env"
set -a
. /app/data/.env
set +x

echo "=> Ensure permissions"
chown -R cloudron:cloudron /run /app/data

echo "=> Start the statping"
exec /usr/local/bin/gosu cloudron:cloudron /app/pkg/statping